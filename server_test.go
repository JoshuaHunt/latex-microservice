package main

import "testing"

func TestBuildDocument(t *testing.T) {
	input := question{"2019U1", "01", 2, 3}

	var actual = buildDocument(input)

	var expected = `
\documentclass[30pt,varwidth,class=scrreprt]{standalone}
\usepackage{gensymb}
\begin{document}
01. Solve the following equation for values of $\theta$ between 0\degree~and
360\degree.
\[  \qquad\qquad 3 \tan \theta +
	 2 \cos \theta = 0\qquad\qquad\qquad\qquad [\textsf{6}]
\]
\end{document}`
	if actual != expected {
		t.Errorf("buildDocument returned %s, expected %s", actual, expected)
	}
}
