package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/rwestlund/gotex"
)

type question struct {
	year     string
	question string
	a        int
	b        int
}

func buildDocument(qn question) string {
	return fmt.Sprintf(`
\documentclass[30pt,varwidth,class=scrreprt]{standalone}
\usepackage{gensymb}
\begin{document}
%s. Solve the following equation for values of $\theta$ between 0\degree~and
360\degree.
\[  \qquad\qquad %d \tan \theta +
	 %d \cos \theta = 0\qquad\qquad\qquad\qquad [\textsf{6}]
\]
\end{document}`, qn.question, qn.b, qn.a)
}

func parseString(v url.Values, name string) (string, error) {
	values, ok := v[name]

	if !ok {
		return "", fmt.Errorf("%s must be set", name)
	}

	if len(values) != 1 {
		return "", fmt.Errorf("expected exactly one %s", name)
	}

	return values[0], nil
}

func parseInt(v url.Values, name string) (int, error) {
	str, err := parseString(v, name)

	if err != nil {
		return 0, err
	}

	return strconv.Atoi(str)
}

/** Parses year and the question from the request URL */
func parseRequest(v url.Values) (question, error) {
	qn, err := parseString(v, "qn")

	if err != nil {
		return *new(question), err
	}

	year, err := parseString(v, "year")

	if err != nil {
		return *new(question), err
	}

	a, err := parseInt(v, "a")

	if err != nil {
		return *new(question), err
	}

	b, err := parseInt(v, "b")

	if err != nil {
		return *new(question), err
	}

	return question{year, qn, a, b}, nil
}

/** Writes the PDF output of the LaTeX document to the filesystem.
 *
 * Returns the filename the PDF was written to. */
func writePdf(document string) (string, error) {
	pdf, err := gotex.Render(document, gotex.Options{
		Command: "/usr/bin/pdflatex",
		Runs:    1})

	if err != nil {
		return "", err
	}

	f, err := ioutil.TempFile("/tmp", "*.pdf")

	if err != nil {
		return "", err
	}

	err = ioutil.WriteFile(f.Name(), pdf, 0644)
	return f.Name(), err
}

func convertToPng(pdfFilename string) ([]byte, error) {
	if !strings.HasSuffix(pdfFilename, ".pdf") {
		return nil, fmt.Errorf("Expected pdf filename but got %s", pdfFilename)
	}
	pngFilename := pdfFilename[0:len(pdfFilename)-len(".pdf")] + ".png"
	log.Printf("Running `convert %s %s`\n", pdfFilename, pngFilename)
	cmd := exec.Command("convert", pdfFilename, pngFilename)
	err := cmd.Run()

	if err != nil {
		return []byte{}, err
	}

	defer os.Remove(pngFilename)
	return ioutil.ReadFile(pngFilename)
}

func respondWithImage(w http.ResponseWriter, image []byte) {
	w.Header().Add("Content-Type", "image/png")
	w.Write(image)
}

func handler(w http.ResponseWriter, r *http.Request) {
	qn, err := parseRequest(r.URL.Query())

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	document := buildDocument(qn)
	pdfFilename, err := writePdf(document)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer os.Remove(pdfFilename)
	image, err := convertToPng(pdfFilename)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	respondWithImage(w, image)
	log.Println("Processed request")
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
