all: build run

build:
	go build
	docker build -t latex-microservice .

run:
	docker run --rm -p 8712:8080 latex-microservice

daemon:
	docker run --rm -d -p 8712:8080 --name latex latex-microservice
