FROM golang:buster as base
RUN apt update
RUN apt install texlive texlive-latex-extra imagemagick -y

FROM golang:1.15 as builder
WORKDIR /go/src/app
COPY go.mod go.sum server.go ./
ARG SKAFFOLD_GO_GCFLAGS
RUN go build -v -gcflags="${SKAFFOLD_GO_GCFLAGS}" -o /go/server server.go

FROM base
WORKDIR /app
COPY imagemagick-policy.xml /etc/ImageMagick-6/policy.xml
COPY --from=builder /go/server .
CMD ["./server"]
